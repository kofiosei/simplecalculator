import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

/**
 * Created by oseik on 1/17/17.
 */
public class NumericCalculator extends SimpleCalculator<Double> {

    public NumericCalculator(){
        super();
    }

    public static void main(String... args) throws Exception {
        NumericCalculator myCalc = new NumericCalculator();
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));
        String line = buffer.readLine();
        String[] tokens = myCalc.tokenizeInput(line);
        myCalc.convertToReversePolish(tokens);
    }


    @Override
    public Double calculate(List<Object> reversePolishForm) {
        for (Object currItem: reversePolishForm){
            if (currItem instanceof Double ){
                calculateStack.push((Double)currItem);
            }
            else {
                Double right = calculateStack.pop();
                Double left = calculateStack.pop();
                if (operatorFunctionMap.containsKey(currItem))
                    calculateStack.push(operatorFunctionMap.get(currItem).apply(left, right));
                else
                    throw new RuntimeException("Unknown or unsupported operator "+ (String)currItem);
            }
        }
        return calculateStack.pop();
    }

    @Override
    public List<String> convertToReversePolish(String[] input) {
        //TODO : some routine that converts infix to reverse polish notation

        //this clearly doesn't do reverse polish but this method should return an arraylist of tokens in reverse polish
        List<String> reversePolish = Arrays.asList(input);
        return reversePolish;
    }


    @Override
    public Double divide(Double left, Double right) {
        return left / right;
    }

    @Override
    public Double add(Double left, Double right) {
        return left + right;
    }

    @Override
    public Double subtract(Double left, Double right) {
        return left - right;
    }

    @Override
    public Double multiply(Double left, Double right) {
        return left * right;
    }

    @Override
    public Double exponent(Double left, Double right) {
        return Math.pow(left, right);
    }
}
