/**
 * Created by oseik on 1/17/17.
 */
public interface Operations<S> {
    abstract S divide (S left , S right);
    abstract S add (S left, S right);
    abstract S subtract (S left, S right);
    abstract S multiply (S left, S right);
    abstract S exponent (S left, S right);
}
