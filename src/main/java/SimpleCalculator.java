import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * Created by oseik on 1/17/17.
 */

//Where T is the type of operand. For numeric values we are using doubles ... but in theory you could have a different
//kind of object which supports the "Operations"
abstract class SimpleCalculator<T> implements Operations<T> {

    protected Map<String, BiFunction<T,T ,T>> operatorFunctionMap = new HashMap<>();

    protected Stack<T> calculateStack = new Stack<T>();

    protected String[] supportedOperators = new String[]{"+", "-", "/", "*", "^"};

    public SimpleCalculator(){
        operatorFunctionMap.put("*", this::multiply);
        operatorFunctionMap.put("+", this::add);
        operatorFunctionMap.put("-", this::subtract);
        operatorFunctionMap.put("/", this::divide);
        operatorFunctionMap.put("^", this::exponent);
    }

    abstract public T calculate(List<Object> reversePolishForm);


    abstract public List<String> convertToReversePolish(String[] someStr);

    public String[] tokenizeInput(String str) {
        //smudge together all the +/*- etc etc to create a regex pattern class ...
        // do regex to pad them all ... then split on strings ... so that calculator will work for input without spaces
        final String regex = Arrays.stream(supportedOperators).collect(Collectors.joining("\\"));
        return str.replaceAll("[" + regex + "]", " $1 ").split("\\s+");

    }
}
